# coding: utf-8

"""
    CrestDB REST API

    Crest Rest Api to manage data for calibration files.  # noqa: E501

    OpenAPI spec version: 2.0
    Contact: andrea.formica@cern.ch
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import crestapi
from crestapi.models.iov_set_dto import IovSetDto  # noqa: E501
from crestapi.rest import ApiException


class TestIovSetDto(unittest.TestCase):
    """IovSetDto unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testIovSetDto(self):
        """Test IovSetDto"""
        # FIXME: construct object with mandatory attributes with example values
        # model = crestapi.models.iov_set_dto.IovSetDto()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
